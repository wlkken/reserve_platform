package com.qf.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.HistoryPrice;

public interface HistoryPriceMapper extends BaseMapper<HistoryPrice> {
}
