package com.qf.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.RoomPrice;

public interface RoomPriceMapper extends BaseMapper<RoomPrice> {
}
