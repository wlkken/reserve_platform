package com.qf.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.Room;

public interface RoomMapper extends BaseMapper<Room> {
}
