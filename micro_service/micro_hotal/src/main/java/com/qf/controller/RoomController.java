package com.qf.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.entity.ResultData;
import com.qf.entity.Room;
import com.qf.entity.RoomPrice;
import com.qf.service.IRoomPriceService;
import com.qf.service.IRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {

    @Autowired
    private IRoomService roomService;
    @Autowired
    private IRoomPriceService roomPriceService;

    @RequestMapping("/insert")
    public ResultData<Boolean> insert(@RequestBody Room room){
        return new ResultData<Boolean>().setData(roomService.save(room));
    }

    @RequestMapping("/list/{hid}")
    public ResultData<List<Room>> list(@PathVariable Integer hid){
        QueryWrapper<Room> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("hid", hid);
        List<Room> list = roomService.list(queryWrapper);
        return new ResultData<List<Room>>().setData(list);
    }

    /**
     * 获得房间价格
     * @return
     */
    @RequestMapping("/getPirce")
    public ResultData<List<RoomPrice>> getRoomPrice(@RequestParam Integer rid){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("rid", rid);
        List<RoomPrice> list = roomPriceService.list(queryWrapper);
        return new ResultData<List<RoomPrice>>().setData(list);
    }

    /**
     * 修改房屋价格
     * @return
     */
    @RequestMapping("/updateprice")
    public ResultData<Boolean> updatePrice(@RequestBody RoomPrice roomPrice){
        boolean flag = roomPriceService.updateById(roomPrice);
        return new ResultData<Boolean>().setData(flag);
    }
}
