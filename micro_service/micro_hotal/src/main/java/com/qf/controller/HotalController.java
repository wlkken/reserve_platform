package com.qf.controller;

import com.qf.entity.Hotal;
import com.qf.entity.ResultData;
import com.qf.service.IHotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 新增酒店
 * 新增（酒店）客房类型 - 新增价格表（30天）
 * 调整每个客房的每天价格
 */
@RestController
@RequestMapping("/hotal")
public class HotalController {

    @Autowired
    private IHotalService hotalService;

    /**
     * 新增酒店
     * @param hotal
     * @return
     */
    @RequestMapping("/insert")
    public ResultData<Boolean> insert(@RequestBody Hotal hotal){
        boolean flag = hotalService.save(hotal);
        return new ResultData<Boolean>().setData(flag);
    }

    /**
     * 查询酒店列表
     * @return
     */
    @RequestMapping("/list")
    public ResultData<List<Hotal>> list(){
        List<Hotal> list = hotalService.list();
        return new ResultData<List<Hotal>>().setData(list);
    }
}
