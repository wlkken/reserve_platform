package com.qf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.entity.RoomPrice;

public interface IRoomPriceService extends IService<RoomPrice> {
}
