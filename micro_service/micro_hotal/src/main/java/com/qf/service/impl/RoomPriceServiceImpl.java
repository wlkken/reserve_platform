package com.qf.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.dao.RoomPriceMapper;
import com.qf.entity.RoomPrice;
import com.qf.service.IRoomPriceService;
import org.springframework.stereotype.Service;

@Service
public class RoomPriceServiceImpl extends ServiceImpl<RoomPriceMapper, RoomPrice> implements IRoomPriceService {
}
