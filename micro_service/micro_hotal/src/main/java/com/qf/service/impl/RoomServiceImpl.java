package com.qf.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.dao.RoomMapper;
import com.qf.entity.Room;
import com.qf.entity.RoomPrice;
import com.qf.service.IRoomPriceService;
import com.qf.service.IRoomService;
import com.qf.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoomServiceImpl extends ServiceImpl<RoomMapper, Room> implements IRoomService {

    @Autowired
    private IRoomPriceService roomPriceService;

    @Override
    @Transactional
    public boolean save(Room room) {

        boolean flag = super.save(room);

        List<RoomPrice> roomPrices = new ArrayList<>();

        //同时生成最近1个月的价格列表
        for (int i = 0; i < 10; i++) {
            RoomPrice roomPrice = new RoomPrice()
                    .setRid(room.getId())
                    .setType(0)//非普通房价
                    .setPrice(room.getDefaultPrice())
                    .setNumber(0)
                    .setHasNumber(room.getNumber())
                    .setDate(DateUtil.getNextDate(i));

            roomPrices.add(roomPrice);
        }

        roomPriceService.saveBatch(roomPrices);
        return flag;
    }
}
