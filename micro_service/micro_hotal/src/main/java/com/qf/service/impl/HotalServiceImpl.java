package com.qf.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.dao.HotalMapper;
import com.qf.entity.City;
import com.qf.entity.Hotal;
import com.qf.entity.ResultData;
import com.qf.feign.CityFeign;
import com.qf.service.IHotalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotalServiceImpl extends ServiceImpl<HotalMapper, Hotal> implements IHotalService {

    @Autowired
    private CityFeign cityFeign;

    /**
     * 保存酒店信息
     * @param entity
     * @return
     */
    @Override
    public boolean save(Hotal entity) {
        boolean flag = super.save(entity);
        //调用城市服务，修改城市对应的数量
        cityFeign.updateCityHNumber(entity.getCid(), 1);
        return flag;
    }

    /**
     * 查询酒店列表
     * @return
     */
    @Override
    public List<Hotal> list() {
        List<Hotal> hotals = super.list();
        for (Hotal hotal : hotals) {
            //获得酒店对应的城市id
            Integer cid = hotal.getCid();
            //获得城市对象
            ResultData<City> cityResultData = cityFeign.queryName(cid);
            //设置城市对象
            hotal.setCity(cityResultData.getData());
        }
        return hotals;
    }
}
