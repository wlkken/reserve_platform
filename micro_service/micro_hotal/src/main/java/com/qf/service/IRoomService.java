package com.qf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.entity.Room;

public interface IRoomService extends IService<Room> {
}
