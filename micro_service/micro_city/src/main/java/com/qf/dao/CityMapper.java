package com.qf.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.City;
import org.apache.ibatis.annotations.Param;

public interface CityMapper extends BaseMapper<City> {

    int updateCityHNUmber(@Param("cid") Integer cid, @Param("number") Integer number);
}
