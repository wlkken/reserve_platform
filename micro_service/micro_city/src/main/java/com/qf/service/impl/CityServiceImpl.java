package com.qf.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qf.dao.CityMapper;
import com.qf.entity.City;
import com.qf.service.ICityService;
import com.qf.util.PinyinUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City> implements ICityService {

    @Autowired
    private CityMapper cityMapper;

    /**
     * 重写保存方法
     * @param entity
     * @return
     */
    @Override
    public boolean save(City entity) {

        //设置城市的拼音
        String pinyin = PinyinUtil.str2Pinyin(entity.getCityName());
        entity.setCityPinyin(pinyin);

        return super.save(entity);
    }

    /**
     * 修改对应城市的酒店数量
     * @param cid
     * @param number
     * @return
     *
     * 线程1 -> 开启事务 -> 加锁 -> 业务 -> 释放锁 -> 提交事务
     * 线程2 -> 开启事务 -> 加锁 -> 业务
     */
    @Override
    @Transactional
    public int updateCityHNumber(Integer cid, Integer number) {
        return cityMapper.updateCityHNUmber(cid, number);
    }
}
