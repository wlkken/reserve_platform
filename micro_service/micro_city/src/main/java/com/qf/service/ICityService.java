package com.qf.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.entity.City;

public interface ICityService extends IService<City> {

    int updateCityHNumber(Integer cid, Integer number);
}
