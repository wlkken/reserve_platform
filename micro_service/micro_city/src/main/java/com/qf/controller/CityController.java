package com.qf.controller;

import com.qf.entity.City;
import com.qf.entity.ResultData;
import com.qf.service.ICityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/city")
@Slf4j
public class CityController {

    @Autowired
    private ICityService cityService;

    /**
     * 添加城市接口
     * @return
     */
    @RequestMapping("/save")
    public ResultData<Boolean> citySave(@RequestBody City city){
        log.info("添加城市：" + city);
        boolean flag = cityService.save(city);
        return new ResultData<Boolean>().setData(flag);
    }

    /**
     * 城市列表
     * {
     * code: ,
     * msg:,
     * data: xxxxx
     * }
     *
     * @return
     */
    @RequestMapping("/list")
    public ResultData<List<City>> cityList(){
        List<City> list = cityService.list();
        return new ResultData<List<City>>().setData(list);
    }

    /**
     * 修改城市对应的酒店数量
     * @param cid
     * @param number
     * @return
     */
    @RequestMapping("/update/{cid}/{number}")
    public ResultData<Boolean> updateCityHNumber(@PathVariable Integer cid, @PathVariable Integer number){
        int result = cityService.updateCityHNumber(cid, number);
        return new ResultData<Boolean>().setData(result > 0);
    }

    @RequestMapping("/queryName/{cid}")
    public ResultData<City> queryName(@PathVariable Integer cid){
        City city = cityService.getById(cid);
        return new ResultData<City>().setData(city);
    }
}
