package com.qf.controller;

import com.qf.entity.ResultData;
import com.qf.entity.Room;
import com.qf.entity.RoomPrice;
import com.qf.feign.HotalFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/system")
@Slf4j
public class SystemRoomController {

    @Autowired
    private HotalFeign hotalFeign;

    /**
     * 跳转到客房管理页面
     * @return
     */
    @RequestMapping("/toroomadd")
    public String toroomadd(){
        return "roomadd";
    }

    /**
     * 保存客房信息
     * @return
     */
    @RequestMapping("/insertroom")
    public String insertroom(Room room){
        hotalFeign.insertRoom(room);
        return "redirect:/system/hotallist";
    }

    /**
     * 客房管理
     * @return
     */
    @RequestMapping("/roomlist")
    public String roomlist(Integer hid, Model model){
        ResultData<List<Room>> room = hotalFeign.getRoom(hid);
        model.addAttribute("rooms", room.getData());
        return "roomlist";
    }

    /**
     * 跳转到房价更新页面
     * @return
     */
    @RequestMapping("/toupdateprice")
    public String toupdateprice(Integer rid, Model model){
        ResultData<List<RoomPrice>> roomPrice = hotalFeign.getRoomPrice(rid);
        model.addAttribute("roomprices", roomPrice.getData());
        return "roompricelist";
    }

    /**
     * 修改房间价格
     * @return
     */
    @RequestMapping("/updatepirce")
    public String updatepirce(RoomPrice roomPrice){
        hotalFeign.updatePrice(roomPrice);
        return "redirect:/system/toupdateprice?rid=" + roomPrice.getRid();
    }
}
