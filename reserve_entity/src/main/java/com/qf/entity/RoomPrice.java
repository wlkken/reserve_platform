package com.qf.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

@Data
@Accessors(chain = true)
public class RoomPrice implements Serializable {

  @TableId(type = IdType.AUTO)
  private Integer id;
  private Integer rid;
  private Date date;
  private BigDecimal price;
  private Integer type;
  private Integer number;
  private Integer hasNumber;

}
