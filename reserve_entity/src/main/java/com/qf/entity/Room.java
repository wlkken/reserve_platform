package com.qf.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class Room implements Serializable {

  @TableId(type = IdType.AUTO)
  private Integer id;
  private Integer hid;
  private String title;
  private String area;
  private Integer num;
  private String image;
  private String info;
  private String bed;
  private Integer number;
  private BigDecimal defaultPrice;

}
