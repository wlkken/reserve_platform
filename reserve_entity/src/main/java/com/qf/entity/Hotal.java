package com.qf.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.*;

@Data
@Accessors(chain = true)
public class Hotal implements Serializable {

  @TableId(type = IdType.AUTO)
  private Integer id;
  private String hotalName;
  private String hotalImage;
  private Integer type;
  private String hotalInfo;
  private String keyword;
  private double lon;
  private double lat;
  private Integer star;
  private String brand;
  private String address;
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date openTime;
  private Integer cid;
  private String district;
  private Date createTime = new Date();
  private Integer status = 0;

  @TableField(exist = false)
  private City city;

}
