package com.qf.entity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.*;

@Data
@Accessors(chain = true)
public class City implements Serializable {

  @TableId(type = IdType.AUTO)
  private Integer id;
  private String cityName;
  private String cityPinyin;
  private Integer hotalNumber;
  private Date createTime;
  private Integer status;

}
