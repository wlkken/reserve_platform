package com.qf.feign;

import com.qf.entity.City;
import com.qf.entity.ResultData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient("micro-city")
public interface CityFeign {

    @RequestMapping("/city/save")
    ResultData<Boolean> citySave(@RequestBody City city);

    @RequestMapping("/city/list")
    ResultData<List<City>> cityList();

    @RequestMapping("/city/update/{cid}/{number}")
    ResultData<Boolean> updateCityHNumber(@PathVariable Integer cid, @PathVariable Integer number);

    @RequestMapping("/city/queryName/{cid}")
    ResultData<City> queryName(@PathVariable Integer cid);
}
