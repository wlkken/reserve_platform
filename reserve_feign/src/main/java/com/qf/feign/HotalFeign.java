package com.qf.feign;

import com.qf.entity.Hotal;
import com.qf.entity.ResultData;
import com.qf.entity.Room;
import com.qf.entity.RoomPrice;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("micro-hotal")
public interface HotalFeign {

    @RequestMapping("/hotal/insert")
    ResultData<Boolean> insert(@RequestBody Hotal hotal);

    @RequestMapping("/hotal/list")
    ResultData<List<Hotal>> list();

    @RequestMapping("/room/insert")
    ResultData<Boolean> insertRoom(@RequestBody Room room);

    @RequestMapping("/room/list/{hid}")
    ResultData<List<Room>> getRoom(@PathVariable Integer hid);

    @RequestMapping("/room/getPirce")
    ResultData<List<RoomPrice>> getRoomPrice(@RequestParam Integer rid);

    @RequestMapping("/room/updateprice")
    ResultData<Boolean> updatePrice(@RequestBody RoomPrice roomPrice);
}
