package com.qf.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    /**
     * 获得next天之后的日期
     * @param next
     * @return
     */
    public static Date getNextDate(int next){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, next);
        return calendar.getTime();
    }
}
